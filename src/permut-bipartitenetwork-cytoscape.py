#!/usr/bin/env python3
#permut-bipartitenetwork-cytoscape.py
# -*- coding: utf-8 -*-
##########################
# This script will generate N random queries to Cytoscape.
# From those N queries, it will compute the inter-group degree.
# This script was specially designed for Montal et al 2021
# The idea was to create a null-distribution of degree between pTau genes
# and 577 random genes from the ABA set.
#########################

# ----------------------------------------------
# Import Libraries
import os
import argparse
from os.path import join

import pandas as pd
import numpy as np
import py4cytoscape as p4c

import pdb


# ----------------------------------------------
# - Parser input options
def options_parse():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    HELPTEXT = """

    permut-bipartitenetwork-cytoscape.py[dev version]

    This script will generate N random queries to Cytoscape.
    From those N queries, it will compute the inter-group degree.
    This script was specially designed for Montal et al 2021
    The idea was to create a null-distribution of degree between pTau genes
    and 577 random genes from the ABA set.


    INPUTS:
    -------



    Author:
    ------
    Victor Montal
    victor.montal [at] protonmail [dot] com

    """

    USAGE = """

    """

    parser = argparse.ArgumentParser(description = HELPTEXT)
    # help text
    h_staticset  = 'Path to .txt file gene list (one-by-row) that will be fixed (non-permuted)'
    h_genedb     = 'Path to .txt file gene list (one-by-row) with all the genes (all AHBA set)'
    h_ngenes     = 'Number of genes to randomly select from all genes list'
    h_permut     = 'Number permutations (default=500)'
    h_topgenes   = 'Number top genes to query to GeneMania (default=20)'
    h_outdir     = 'Path where permutations will be saved'

    parser.add_argument('--static-genes',       dest='fstaticg',        help=h_staticset,   required = True)
    parser.add_argument('--full-list-genes',    dest='fallgene',        help=h_genedb,      required = True)
    parser.add_argument('--gene-subset',        dest='numqueryg',       help=h_ngenes,      required = False, default=20)
    parser.add_argument('--permut',             dest='permut',          help=h_permut,      required = False, default=500)
    parser.add_argument('--top-genes',          dest='topgenemania',    help=h_topgenes,    required = True)
    parser.add_argument('--outdir',             dest='outdir',          help=h_outdir,      required = True)


    args = parser.parse_args()

    # Check options exists
    return args

# - Supplementary functions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()

# - Main function
def permutCytoscape(options):
    """
    1. Load static (reference) genes
    2. Load fullset genes (ABA)
    3. Run NPERM permutations
        3.1 Select NGENES random genes from fullset
        3.2 Query to Genemania (using py4cytoscape library)
        3.3 From results, save cys network, node table and edge table
        3.4 Compute inter-module degree
        3.5 Save into txt the inter-module degree
    """
    print("@ Bipartite network statistical inference")
    permut = np.int(options.permut)
    numqueryg = np.int(options.numqueryg)

    print(". Parameters:")
    print(".    Number permutations: "+options.permut)
    print(".    Number random genes per permutation: "+options.numqueryg)
    print(".    Output path: "+options.outdir+"\n")

    print("@ Loading query file.. ")
    qgenes = pd.read_csv(options.fstaticg)
    qgeneslist = qgenes['geneID'].values

    print(".    #Query set genes: "+str(len(qgenes))+"\n")
    qgenescat = '|'.join(qgenes.iloc[:,0].values)

    print("@ Loading whole gene list.. ")
    allgenes = pd.read_csv(options.fallgene)
    allgenes = allgenes[~allgenes['geneID'].isin(qgeneslist)]   # Delete genes in static query
    print(".    #All genes genes: "+str(len(allgenes))+"\n")



    """
    #UNCOMMET THIS IF YOU WANT TO RUN FULL SET OF RANDOM GENES
    allgenes = pd.read_csv(options.fallgene)
    randpos = np.random.choice(range(len(allgenes)), 121, replace=False)
    qgeneslist = allgenes.iloc[randpos,0].values
    qgenescat = '|'.join(qgeneslist)
    allgenes = allgenes[~allgenes['geneID'].isin(qgeneslist)]
    """

    print("@ Starting the permutation-based null-distribution computation")
    for idx in range(0,permut):
            print(".    Working permutation: "+str(idx))
            idxprint = f'{idx:03d}'
            outpath = join(options.outdir,'permut_'+idxprint)
            os.makedirs(outpath)

            # Acess X random genes from AHBA
            randpos = np.random.choice(range(len(allgenes)), numqueryg, replace=False)
            randgene = allgenes.iloc[randpos,0].values
            randgene = '|'.join(randgene)

            # Concat with static genes
            currqueryg = randgene+'|'+qgenescat

            # Do query to cytoscape
            print(".       Querying Genemania..")
            cytocmd = 'genemania search attrLimit=20 combiningMethod=AUTOMATIC \
                        geneLimit={glim} genes={glist} networks=pi offline=true \
                        organism="H. Sapiens"'
            cytocmd = cytocmd.format(glim=options.topgenemania, glist='"'+currqueryg+'"')
            p4c.commands.commands_run(cytocmd)
            cnet = p4c.get_network_list()[0]

            # Subset to QUERY terms
            print(".       Filtering to ONLY query genes..")
            p4c.filters.create_column_filter('QueryFilter',column='node type',
                                            criterion='query', predicate='IS',
                                            type='nodes', network=cnet)

            # Prepare Node-table and edge-table for further computations
            print(".       Computing inter-module degree..")
            cedges = pd.DataFrame(columns=['Source', 'Target','LinkType'])
            nodeinfo = p4c.get_table_columns(columns=['gene name','shared name', 'node type'])
            nodeinfo = nodeinfo[nodeinfo['node type'] == 'query']
            tmpedges = p4c.networks.get_all_edges(network=cnet)
            for tmpedge in tmpedges:
                cedges = cedges.append({'Source': tmpedge.split('|')[0],
                                        'Target': tmpedge.split('|')[1],
                                        'LinkType': tmpedge.split('|')[2]},
                                        ignore_index=True)

            # Clean links for inter or intra
            nodeinfo['group'] = np.where(nodeinfo['gene name'].isin(qgeneslist), 1, 2)
            nodeinfo['interdegree'] = 0
            nodeinfo.index = nodeinfo['shared name']
            for i, row in cedges.iterrows():
                sgene = row['Source']
                tgene = row['Target']

                # Filter out query to result links
                if (sgene in nodeinfo.index.values) and (tgene in nodeinfo.index.values):
                    sgroup = nodeinfo.loc[sgene,'group']
                    tgroup = nodeinfo.loc[tgene,'group']

                    if sgroup != tgroup:
                        #print(sgene+" "+tgene)
                        #print(str(sgroup)+" "+str(tgroup))
                        nodeinfo.loc[sgene,'interdegree'] = nodeinfo.loc[sgene,'interdegree'] + 1
                        nodeinfo.loc[tgene,'interdegree'] = nodeinfo.loc[tgene,'interdegree'] + 1

            # Compute the average inter-degree
            subsettau = nodeinfo[nodeinfo["group"] == 1]
            subsetrand = nodeinfo[nodeinfo["group"] == 2]

            meaninterstatic = subsettau["interdegree"].mean()
            totalinterstatic = subsettau["interdegree"].sum()
            meaninterrandom = subsetrand["interdegree"].mean()
            totalinterrand = subsetrand["interdegree"].sum()
            countdiffgenesstatic = len(subsettau[subsettau['interdegree'] > 0])
            countdiffgenesrand = len(subsetrand[subsetrand['interdegree'] > 0])

            print(".       Mean Inter-module degree of static genes: "+str(meaninterstatic))
            print(".       Total Inter-module degree of static genes: "+str(totalinterstatic))
            print(".       Mean Inter-module degree of random genes: "+str(meaninterrandom))
            print(".       Total Inter-module degree of random genes: "+str(totalinterrand))
            print(".       Count static genes with link: "+str(countdiffgenesstatic))
            print(".       Count random genes with link: "+str(countdiffgenesrand))


            # Save current network
            print(".       Saving files..")
            tosave = join(outpath,'cyto-session-permut-'+idxprint)
            #p4c.session.save_session(join(tosave,'cyto-session-permut.cys'))
            with open(join(outpath,'static-mean-intermodule.txt'),'w') as f:
                f.write(str(meaninterstatic))
            with open(join(outpath,'static-total-intermodule.txt'),'w') as f:
                f.write(str(totalinterstatic))
            with open(join(outpath,'random-mean-intermodule.txt'),'w') as f:
                f.write(str(meaninterrandom))
            with open(join(outpath,'random-total-intermodule.txt'),'w') as f:
                f.write(str(totalinterrand))
            with open(join(outpath,'static-count-intermodule.txt'),'w') as f:
                f.write(str(countdiffgenesstatic))
            with open(join(outpath,'random-count-intermodule.txt'),'w') as f:
                f.write(str(countdiffgenesrand))

            nodeinfo.to_csv(join(outpath,'node-info-table.csv'))
            cedges.to_csv(join(outpath,'edge-info-table.csv'))

            # Delete network from cytoscape enviroment
            p4c.networks.delete_network(network=cnet)


# - Run code
if __name__=="__main__":
    # Command Line options and error checking done here
    options = options_parse()
    permutCytoscape(options)




#done
