# Ctyo-perm

Permutation-based statistical inference of the relationship between two groups of genes

## Rationale
A network-based approach to compare two set of genes (Set-A and Set-B), is to compute the bipartite network that describes the inter-connection between the two sets. Such network of links can be obtained using Cytoskape and Genemania plugin. Nevertheless, it is important to assess if the retrieved network, and its connectivity profiles, might also occur by change (randomness of links).

ctyo-perm allows to perform several permutations (NPERM) where a set of genes (Set-A) remains static, whereas the Set-B of genes is changed to a random set of genes. The script re-computes the connectivity profile at each permutation (Set-A-static vs Set-B-random) and stores the final results. 

Such procedure allow to create a null-distribution of the connectivity profiles between the two set of genes, and compute its statistical profile.
 
Overall, what we want to answer is: "The connectivity profile between Set-A and Set-B genes, are statistically relevant, or it might be similar to compare two set of random genes?"


## Installation
```bash
git clone https://gitlab.com/vmontalb/ctyo-perm.git
```

## Requirements
- Packages
   - Cytoscape [https://cytoscape.org/](https://cytoscape.org/)
   - Genemania App for Cytoscape [http://apps.cytoscape.org/apps/genemania](http://apps.cytoscape.org/apps/genemania)
- Python (3.5 or above)
    - pandas
    - numpy
    - [py4cytoscape](https://py4cytoscape.readthedocs.io/en/latest/)
    - argparse

## Usage
```python
```

## Tutorial - Case Example


## Output
The script will create *NPERM* folders with files corresponding to each iteration results. Specifically:
1. **node-info-table.csv**
   - Gene information (network nodes) of the genes used to compute the between-group metrics. File includes the group (1 if gene of interest, 2 if random gene), gene-id, genemania gene id and inter-group degree.
2. **edge-info-table.csv**
   - Link information (network edges) of the gene-gene interactions. File also includes type of interaction for each link.
3. **static-count-intermodule.txt**
   - Number of (unique) genes that are connected from the gene-set of interest (static) to the random genes.
4. **static-mean-intermodule.txt**
   - Mean inter-group degree between the gene-set of interest (static) and the random genes.
5. **static-total-intermodule.txt**
   - Total number (SUM) of inter-group degree between the gene-set of interest (static) and the random genes.
6. **random-count-intermodule.txt**
   - Number of (unique) genes that are connected from the random set of genes to the gene-set of interest (static).
7. **random-mean-intermodule.txt**
   - Mean inter-group degree between the random set of genes and the gene-set of interest (static).
8. **random-total-intermodule.txt**
   - Total number (SUM) of inter-group degree between the random set of genes and the gene-set of interest (static).


One can merge/concatenate the information into a single file, and plot it using python.
```python
import numpy as np
import seaborn as sns

# Define path
permutp = 'path/to/outputs'

# Define apriori values
static2randmean = 0.78
static2randtotal = 491
static2randunique = 219

numpermut = 500
meanstaticinter = np.zeros([1,numpermut])
countstaticinter = np.zeros([1,numpermut])
sumstaticinter = np.zeros([1,numpermut])

# Iteratively load from each permutation
for idx in range(numpermut):
    idxprint = f'{idx:03d}'
    cpermutp = join(permutp,'permut_'+idxprint)
    countrandinter[0,idx] = np.loadtxt(join(cpermutp,'random-count-intermodule.txt'))
    meanstaticinter[0,idx] = np.loadtxt(join(cpermutp,'static-mean-intermodule.txt'))
    sumstaticinter[0,idx] = np.loadtxt(join(cpermutp,'static-total-intermodule.txt'))

# Prepare 3 subplot  seaborn
fig, axs = plt.subplots(1,3, figsize=(18,9))
fig.suptitle('Distribution of Static vs random')  

# Plot
dat = pd.DataFrame({'Static Mean inter-module Degree':meanstaticinter[0,:]})    
tt = sns.histplot(dat, bins=50, kde = True, ax=axs[0])
axs[0].axvline(static2randmean, color='r', linestyle='dashed')

dat = pd.DataFrame({'Static Number inter-group unique genes':countrandinter[0,:]})    
sns.histplot(dat, bins=50, kde = True, ax=axs[1])
axs[1].axvline(static2randtotal, color='r', linestyle='dashed')

dat = pd.DataFrame({'Static Sum total inter-module Degree':sumstaticinter[0,:]})    
sns.histplot(dat, bins=50, kde = True, ax=axs[2])
axs[2].axvline(static2randunique, color='r', linestyle='dashed')
```

![Semantic description of image](/img/cyto-perm-dist.png "Permutation histograms")
